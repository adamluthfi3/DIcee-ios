//
//  ViewController.swift
//  Dicee
//
//  Created by Great Education on 7/17/18.
//  Copyright © 2018 adamluthfi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var randomDiceIndex1: Int = 0
    var randomDiceIndex2: Int = 0
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    
    let diceArray = ["dice1","dice2","dice3","dice4","dice5","dice6"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        updateDiceImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func rollButtonPressed(_ sender: UIButton) {
        
        updateDiceImage()
    }
    
    fileprivate func updateDiceImage() {
        self.randomDiceIndex1 = Int(arc4random_uniform(6))
        self.randomDiceIndex2 = Int(arc4random_uniform(6))
        
        self.diceImageView1.image = UIImage(named: diceArray[self.randomDiceIndex1])
        self.diceImageView2.image = UIImage(named: diceArray[self.randomDiceIndex2])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        updateDiceImage()
    }
    
}

